#!/bin/bash

webroot="/Users/fuzzy76/public_html"
mysqlu="root"
mysqlp="root"
legacydb="hk_slave"
masterdb="profil"
legacyplatform="helsekompetanse.no"
drushphp='/usr/local/Cellar/drush/6.1.0/libexec/drush.php' # /usr/share/drush/drush.php on prod

# Make and install
cd $webroot
drush make http://hk1.tj.unn.no/makefiles/profil.make profil
cd profil
drush --yes site-install --account-mail=teknisk@helsekompetanse.no --db-url=mysql://$mysqlu:$mysqlp@localhost/$masterdb --site-mail=noreply@helsekompetanse.no --site-name=Profil --sites-subdir=profil.helsekompetanse.no
cd sites/profil.helsekompetanse.no

# Enable core and contrib
drush --yes en admin_views auto_username bakery block_class bundle_copy context context_ui context_no_title cpn ctools devel elements entity fences front_page googleanalytics l10n_update locale jquery_placeholder legal libraries local_tasks_blocks logintoboggan menu_block migrate migrate_ui mollom realname rest_server retina_images services token views views_bulk_operations views_ui wysiwyg

# Enable custom
drush --yes en hk_login hk_sso_update hk_user hk_user_ui hksitebar hk_migrate_users

# Set picture path
drush vset user_picture_path "pictures/profiles"

# Mollom
drush vset mollom_private_key 74d7988ad4aa857a9389e61a8571582b
drush vset mollom_public_key 5f9dfcafa9cd81524ed3f542868fbb63
drush vset mollom_fallback 0
drush vset mollom_privacy_link 0

# Realname
drush vset realname_pattern "[user:field_first_name] [user:field_last_name]"

# Logintoboggan
drush vset logintoboggan_login_with_email 1
drush vset logintoboggan_immediate_login_on_register 1

# Bakery
drush vset bakery_is_master 1
drush vset bakery_master "http://profil.helsekompetanse.no/"
drush php-eval "variable_set('bakery_slaves', array('http://helsekompetanse.no/'));"
drush vset bakery_key "MCwwDQYJKoZIhvcNAQEBBQADGwAwGAIRALQpDXgN+z4nXhg7e4H4mhECAwEAAQ=="
drush vset bakery_domain ".helsekompetanse.no"
drush php-eval "variable_set('bakery_supported_fields', array('name' => 'name', 'mail' => 'mail', 'status' => 'status', 'picture' => 'picture', 'language' => 'language', 'signature' => 'signature'));"

# services setup
drush role-create 'internal services'
drush role-add-perm 'internal services' 'access user profiles'
drush role-add-perm 'internal services' 'administer Terms and Conditions'
drush role-add-perm 'internal services' 'bypass bakery'

# Google analytics
drush vset googleanalytics_account "UA-3400138-1"
drush php-eval "variable_set('googleanalytics_roles', array( 3 => '3', 4 => '4', 5 => '5', 1 => 0, 2 => 0));"
drush vset googleanalytics_visibility_roles 1

# Front page
drush vset front_page_enable 1

# Code Per Node
drush vset cpn_syntax_highlightning "codemirror"
drush vset cpn_path "public://cpn"

# Legal module
mysqldump -u$mysqlu -p$mysqlp --no-create-info $legacydb legal_conditions | mysql -u $mysqlu -p$mysqlp $masterdb

# Auto username (can this stay or not?)
drush vset --always-set aun_pattern '[user:mail]' ; drush vset --always-set aun_punctuation_ampersand '2' ; drush vset --always-set aun_punctuation_asterisk '2' ; drush vset --always-set aun_punctuation_at '2' ; drush vset --always-set aun_punctuation_backtick '2' ; drush vset --always-set aun_punctuation_back_slash '2' ; drush vset --always-set aun_punctuation_caret '2' ; drush vset --always-set aun_punctuation_colon '2' ; drush vset --always-set aun_punctuation_comma '2' ; drush vset --always-set aun_punctuation_dollar '2' ; drush vset --always-set aun_punctuation_double_quotes '2' ; drush vset --always-set aun_punctuation_equal '2' ; drush vset --always-set aun_punctuation_exclamation '2' ; drush vset --always-set aun_punctuation_greater_than '2' ; drush vset --always-set aun_punctuation_hash '2' ; drush vset --always-set aun_punctuation_hyphen '2' ; drush vset --always-set aun_punctuation_left_curly '2' ; drush vset --always-set aun_punctuation_left_parenthesis '2' ; drush vset --always-set aun_punctuation_left_square '2' ; drush vset --always-set aun_punctuation_less_than '2' ; drush vset --always-set aun_punctuation_percent '2' ; drush vset --always-set aun_punctuation_period '2' ; drush vset --always-set aun_punctuation_pipe '2' ; drush vset --always-set aun_punctuation_plus '2' ; drush vset --always-set aun_punctuation_question_mark '2' ; drush vset --always-set aun_punctuation_quotes '2' ; drush vset --always-set aun_punctuation_right_curly '2' ; drush vset --always-set aun_punctuation_right_parenthesis '2' ; drush vset --always-set aun_punctuation_right_square '2' ; drush vset --always-set aun_punctuation_semicolon '2' ; drush vset --always-set aun_punctuation_slash '2' ; drush vset --always-set aun_punctuation_tilde '2' ; drush vset --always-set aun_punctuation_underscore '2'

# Migration parameters
drush php-eval 'variable_set("legacy_database_connection", array("database" => "'$legacydb'", "username" => "'$mysqlu'", "password" => "'$mysqlp'", "host" => "127.0.0.1", "driver" => "mysql"));'
drush vset legacy_destination_directory public://users
drush vset legacy_source_directory $webroot/$legacyplatform

# Leftovers?
drush role-create 'editor'

echo "This is where you follow part 1 of the documentation and press enter here when you are done"
read -n 1 -s
# ---------------------------------------------------------

# Translated mails
drush sqlq "DELETE FROM variable WHERE name LIKE 'user_mail_%_body'; DELETE FROM variable WHERE name LIKE 'user_mail_%_subject';"

# Save dump
drush cc all
drush sql-dump --result-file=/tmp/profildump.sql

# Slave prep
drush --db-url=mysql://$mysqlu:$mysqlp@localhost/$legacydb sqlq "UPDATE users SET name = mail"

# Do migrations
drush ms
drush mi HelsekompetanseUserProfilePicture
drush mi HelsekompetanseUsers

# Legal module
mysqldump -u$mysqlu -p$mysqlp --no-create-info $legacydb legal_accepted | mysql -u root -proot $masterdb

# Services user
drush user-create slave@helsekompetanse.no --mail="slave@helsekompetanse.no" --password="B7BFsSAApJ"
drush user-add-role "internal services" slave@helsekompetanse.no
drush php-eval "\$u=user_load_by_name('slave@helsekompetanse.no');\$l=array('version'=>1,'revision'=>1,'language'=>'nb','uid'=>\$u->uid,'accepted'=>time());drupal_write_record('legal_accepted',\$l);"

# Change to slave
cd $webroot/$legacyplatform/sites/helsekompetanse.no

# Upgrade some modules
drush --yes dis og_massadd
rm -rf ../all/modules/contrib/og_massadd
cd ../all/modules/custom
rm -rf hkprofile
git clone git@bitbucket.org:Helsekompetanse/hkprofile.git
rm -rf hksitebar
git clone https://bitbucket.org/Helsekompetanse/hksitebar.git
rm -rf reqcertlogin
git clone git@bitbucket.org:Helsekompetanse/reqcertlogin.git
git clone git@bitbucket.org:Helsekompetanse/og_massadd.git
cd $webroot/$legacyprofile/sites/helsekompetanse.no
drush --yes en hkprofile og_massadd

# Swap out our login modules
drush --yes dis logintoboggan legal
drush --yes pm-uninstall logintoboggan legal
drush --yes en bakery

# Bakery
drush php-eval 'variable_set("bakery_supported_fields", array("name" => "name", "mail" => "mail", "status" => "status", "picture" => "picture", "language" => "language", "signature" => "signature"));'
drush vset bakery_master 'http://profil.helsekompetanse.no/'
drush vset bakery_key 'MCwwDQYJKoZIhvcNAQEBBQADGwAwGAIRALQpDXgN+z4nXhg7e4H4mhECAwEAAQ=='
drush vset bakery_domain '.helsekompetanse.no'

# HKProfile needs this
drush vset hkprofile_service_account_username 'slave@helsekompetanse.no'
drush vset hkprofile_service_account_password 'B7BFsSAApJ'
drush vset hkprofile_php `which php`
drush vset hkprofile_drush_php_script $drushphp
drush vset hkprofile_master_root $webroot/profil
drush vset hkprofile_master_uri 'http://profil.helsekompetanse.no/'

echo "This is where you follow part 2 of the documentation and press enter here when you are done"
read -n 1 -s

drush vset site_403 "403"

echo "Done!"
