; This is the makefile for helsekompetanse.no

core = 7.x

api = 2
projects[drupal][version] = "7.69"

; Modules
projects[addthis][subdir] = "contrib"
projects[addthis][version] = "4.0-alpha6"
projects[admin_menu][subdir] = "contrib"
projects[admin_menu][version] = "3.0-rc6"
projects[admin_views][subdir] = "contrib"
projects[admin_views][version] = "1.7"
projects[adminimal_admin_menu][subdir] = "contrib"
projects[adminimal_admin_menu][version] = "1.9"
projects[advagg][subdir] = "contrib"
projects[advagg][version] = "2.24"
projects[auto_nodetitle][subdir] = "contrib"
projects[auto_nodetitle][version] = "1.0"
projects[backup_migrate][subdir] = "contrib"
projects[backup_migrate][version] = "3.6"
projects[bakery][subdir] = "contrib"
projects[bakery][version] = "2.0-alpha4"
projects[bakery][patch][] = "https://www.drupal.org/files/1916610-bakery-validate-d7.patch"
projects[block_class][subdir] = "contrib"
projects[block_class][version] = "2.4"
projects[breakpoints][subdir] = "contrib"
projects[breakpoints][version] = "1.6"
projects[ctools][subdir] = "contrib"
projects[ctools][version] = "1.15"
projects[chosen][subdir] = "contrib"
projects[chosen][version] = "2.1"
projects[conditional_fields][subdir] = "contrib"
projects[conditional_fields][version] = "3.0-alpha2"
projects[cpn][subdir] = "contrib"
projects[cpn][version] = "1.7"
projects[date][subdir] = "contrib"
projects[date][version] = "2.10"
projects[devel][subdir] = "contrib"
projects[devel][version] = "1.7"
projects[diff][subdir] = "contrib"
projects[diff][version] = "3.4"
projects[ds][subdir] = "contrib"
projects[ds][version] = "2.16"
projects[elasticsearch_connector][subdir] = "contrib"
projects[elasticsearch_connector][version] = "1.0-alpha9"
projects[elements][subdir] = "contrib"
projects[elements][version] = "1.5"
projects[email][subdir] = "contrib"
projects[email][version] = "1.3"
projects[entity][subdir] = "contrib"
projects[entity][version] = "1.9"
projects[entity_modified][subdir] = "contrib"
projects[entity_modified][version] = "1.2"
projects[entityreference][subdir] = "contrib"
projects[entityreference][version] = "1.5"
projects[entityreference_prepopulate][subdir] = "contrib"
projects[entityreference_prepopulate][version] = "1.7"
projects[environment][subdir] = "contrib"
projects[environment][version] = "1.0"
projects[facetapi][subdir] = "contrib"
projects[facetapi][version] = "1.6"
projects[facetapi_pretty_paths][subdir] = "contrib"
projects[facetapi_pretty_paths][version] = "1.4"
projects[features_extra][subdir] = "contrib"
projects[features_extra][version] = "1.0"
projects[features][subdir] = "contrib"
projects[features][version] = "2.11"
; Not sure if this bug is introduced in 1.0-beta13 but can skip 7.x-1.1
; for now and/or wait for a merged fix. -jhh 20191031.
; https://www.drupal.org/project/field_collection/issues/3091188
projects[field_collection][subdir] = "contrib"
projects[field_collection][version] = "1.0-beta13"
projects[field_group][subdir] = "contrib"
projects[field_group][version] = "1.6"
projects[file_entity][subdir] = "contrib"
projects[file_entity][version] = "2.27"
projects[fitvids][subdir] = "contrib"
projects[fitvids][version] = "1.17"
projects[fontyourface][subdir] = "contrib"
projects[fontyourface][version] = "2.8"
; Maintainer seems to have dropped support for 1.x for some reason.
; Unsure if it's worth upgrading to 2.0 if it's not broken.
; -jhh 20191031
projects[form_builder][subdir] = "contrib"
projects[form_builder][version] = "1.22"
;
; Avventer oppdatering av geocoder til 1.4 da det er ganske mange
; endringer som er tatt inn i september: https://www.drupal.org/project/geocoder/releases/7.x-1.4
; En håndfull åpne saker for 1.4 her: https://www.drupal.org/project/issues/geocoder?text=&status=Open&priorities=All&categories=All&version=7.x-1.4&component=All
; Regner med at det er greit å oppdatere neste gang. -jhh.
; Prøver denne gang. Ikke noe kritisk nytt i issuekøa. -- TIS
projects[geocoder][subdir] = "contrib"
projects[geocoder][version] = "1.4"
projects[geophp][subdir] = "contrib"
projects[geophp][version] = "1.7"
projects[getlocations][subdir] = "contrib"
projects[getlocations][version] = "1.17"
projects[google_analytics][subdir] = "contrib"
projects[google_analytics][version] = "2.6"
projects[httprl][subdir] = "contrib"
projects[httprl][version] = "1.14"
projects[hotjar][subdir] = "contrib"
projects[hotjar][version] = "1.3"
projects[icomoon][subdir] = "contrib"
projects[icomoon][version] = "1.0"
projects[icon][subdir] = "contrib"
projects[icon][version] = "1.0"
projects[imagecache_token][subdir] = "contrib"
projects[imagecache_token][version] = "1.0"
projects[jquery_update][subdir] = "contrib"
projects[jquery_update][version] = "2.7"
projects[label_help][subdir] = "contrib"
projects[label_help][version] = "1.2"
;
; Avventet oppdatering litt så endringene kan modnes, ny funksjonalitet i 2.4. -JHH.
; https://www.drupal.org/project/libraries/releases/7.x-2.4
projects[libraries][subdir] = "contrib"
projects[libraries][version] = "2.5"
projects[link][subdir] = "contrib"
projects[link][version] = "1.7"
projects[local_tasks_blocks][subdir] = "contrib"
projects[local_tasks_blocks][version] = "2.2"
projects[magnific_popup][subdir] = "contrib"
projects[magnific_popup][version] = "1.3"
projects[media][subdir] = "contrib"
projects[media][version] = "2.24"
projects[media_vimeo][subdir] = "contrib"
projects[media_vimeo][version] = "2.1"
projects[menu_block][subdir] = "contrib"
projects[menu_block][version] = "2.8"
projects[menu_token][subdir] = "contrib"
projects[menu_token][version] = "1.0-beta7"
; Metatag 7.x-1.26 is huge and recent, postponed for now. -jhh 20200102
; Details: https://www.drupal.org/project/metatag/releases/7.x-1.26
projects[metatag][subdir] = "contrib"
projects[metatag][version] = "1.25"
projects[modernizr][subdir] = "contrib"
projects[modernizr][version] = "3.11"
projects[module_filter][subdir] = "contrib"
projects[module_filter][version] = "2.2"
projects[node_page_disable][subdir] = "contrib"
projects[node_page_disable][version] = "1.0"
projects[nodequeue][subdir] = "contrib"
projects[nodequeue][version] = "2.2"
projects[oauth][subdir] = "contrib"
projects[oauth][version] = "3.4"
projects[og][subdir] = "contrib"
projects[og][version] = "2.10"
projects[options_element][subdir] = "contrib"
projects[options_element][version] = "1.12"
projects[panels][subdir] = "contrib"
projects[panels][version] = "3.9"
projects[pathauto][subdir] = "contrib"
projects[pathauto][version] = "1.3"
projects[picture][subdir] = "contrib"
projects[picture][version] = "2.13"
projects[redirect][subdir] = "contrib"
projects[redirect][version] = "1.0-rc3"
projects[rules][subdir] = "contrib"
projects[rules][version] = "2.12"
projects[schemaorg][subdir] = "contrib"
projects[schemaorg][version] = "1.0-rc1"
projects[search_api][subdir] = "contrib"
projects[search_api][version] = "1.26"
projects[search_api_db][subdir] = "contrib"
projects[search_api_db][version] = "1.7"
projects[search_api_solr][subdir] = "contrib"
projects[search_api_solr][version] = "1.15"
projects[strongarm][subdir] = "contrib"
projects[strongarm][version] = "2.0"
projects[token][subdir] = "contrib"
projects[token][version] = "1.7"
projects[transliteration][subdir] = "contrib"
projects[transliteration][version] = "3.2"
projects[ultimate_cron][subdir] = "contrib"
projects[ultimate_cron][version] = "2.8"
projects[uuid][subdir] = "contrib"
projects[uuid][version] = "1.3"
projects[varnish][subdir] = "contrib"
projects[varnish][version] = "1.4"
projects[views][subdir] = "contrib"
projects[views][version] = "3.23"
projects[views_block_filter_block][subdir] = "contrib"
projects[views_block_filter_block][version] = "1.0-beta2"
projects[views_bulk_operations][subdir] = "contrib"
projects[views_bulk_operations][version] = "3.5"
projects[webform][subdir] = "contrib"
projects[webform][version] = "3.29"
projects[webform_default_fields][subdir] = "contrib"
projects[webform_default_fields][version] = "3.6"
; Follows the major version of webform! https://www.drupal.org/project/webform_default_fields
projects[webform_tokens][subdir] = "contrib"
projects[webform_tokens][version] = "1.8"
projects[wysiwyg][subdir] = "contrib"
projects[wysiwyg][version] = "2.6"
projects[xautoload][subdir] = "contrib"
projects[xautoload][version] = "5.2"

; Themes
projects[zen][type] = "theme"
projects[zen][version] = "5.6"


; Custom modules
projects[res_images][type] = "module"
projects[res_images][download][type] = "git"
projects[res_images][download][url] = "https://github.com/oleanti/res_images.git"

projects[some_field][type] = "module"
projects[some_field][download][type] = "git"
projects[some_field][download][url] = "https://github.com/oleanti/some_field.git"

projects[varor][subdir] = "custom"
projects[varor][type] = "module"
projects[varor][download][type] = "git"
projects[varor][download][url] = "https://bitbucket.org/Helsekompetanse/varor.git"
projects[varor][download][tag] = "sprint64"

projects[hk_environments][subdir] = "custom"
projects[hk_environments][type] = "module"
projects[hk_environments][download][type] = "git"
projects[hk_environments][download][url]="git@bitbucket.org:Helsekompetanse/hk_environments.git"

; Libraries
; Please fill the following out. Type may be one of get, git, bzr or svn,
; and url is the url of the download.
libraries[chosen][download][type] = "file"
libraries[chosen][download][url] = "https://github.com/harvesthq/chosen/releases/download/v1.1.0/chosen_v1.1.0.zip"
libraries[chosen][directory_name] = "chosen"
libraries[chosen][type] = "library"

; Please fill the following out. Type may be one of get, git, bzr or svn,
; and url is the url of the download.
libraries[ckeditor][download][type] = "file"
libraries[ckeditor][download][url] = "https://download.cksource.com/CKEditor/CKEditor/CKEditor%204.9.2/ckeditor_4.9.2_full.zip"
libraries[ckeditor][directory_name] = "ckeditor"
libraries[ckeditor][type] = "library"

; Please fill the following out. Type may be one of get, git, bzr or svn,
; and url is the url of the download.
libraries[Elastica][download][type] = "file"
libraries[Elastica][download][url] = "https://github.com/elasticsearch/elasticsearch/archive/master.zip"
libraries[Elastica][directory_name] = "Elastica"
libraries[Elastica][type] = "library"

; Please fill the following out. Type may be one of get, git, bzr or svn,
; and url is the url of the download.
libraries[fitvids][download][type] = "file"
libraries[fitvids][download][url] = "https://github.com/davatron5000/FitVids.js/archive/master.zip"
libraries[fitvids][directory_name] = "fitvids"
libraries[fitvids][type] = "library"

; Libraries
libraries[modernizr][download][type] = "file"
libraries[modernizr][download][url] = "https://bitbucket.org/Helsekompetanse/makefiles/raw/79d64cc572da8ef3a1ea509eb19ad999a94d270b/files/libraries/modernizr.js"

libraries[hkapi][download][type] = "git"
libraries[hkapi][download][url] = "git@bitbucket.org:Helsekompetanse/hkapi.git"

; Please fill the following out. Type may be one of get, git, bzr or svn,
; and url is the url of the download.
libraries[codemirror][download][type] = "file"
libraries[codemirror][download][url] = "http://codemirror.net/codemirror.zip"
libraries[codemirror][directory_name] = "codemirror"
libraries[codemirror][type] = "library"

; Please fill the following out. Type may be one of get, git, bzr or svn,
; and url is the url of the download.
libraries[magnific-popup][download][type] = "file"
libraries[magnific-popup][download][url] = "https://github.com/dimsemenov/Magnific-Popup/archive/master.zip"
libraries[magnific-popup][directory_name] = "magnific-popup"
libraries[magnific-popup][type] = "library"

; Please fill the following out. Type may be one of get, git, bzr or svn,
; and url is the url of the download.
libraries[underscore][download][type] = "file"
libraries[underscore][download][url] = "https://github.com/jashkenas/underscore/archive/master.zip"
libraries[underscore][directory_name] = "underscore"
libraries[underscore][type] = "library"

; Please fill the following out. Type may be one of get, git, bzr or svn,
; and url is the url of the download.
libraries[getlocations][download][type] = "file"
libraries[getlocations][download][url] = "https://bitbucket.org/Helsekompetanse/makefiles/raw/470e8cf6dc1f0529b77f8bb9b5004ba3988db0f0/libraries/getlocations-markers.zip"
libraries[getlocations][directory_name] = "getlocations/markers"
libraries[getlocations][type] = "library"

; Please fill the following out. Type may be one of get, git, bzr or svn,
; and url is the url of the download.
libraries[backbone][download][type] = "file"
libraries[backbone][download][url] = "https://github.com/jashkenas/backbone/archive/master.zip"
libraries[backbone][directory_name] = "backbone"
libraries[backbone][type] = "library"
