#Makefiles#

## Slave.make ##

``slave.make`` is the make-file we use to create course sites in our Bakery master/slave-setup. This file will download everything that is needed to create a site, including our installation profile (ee repository https://bitbucket.org/Helsekompetanse/profile-course).

On any site, run the following command:

    drush make https://bitbucket.org/Helsekompetanse/makefiles/raw/sprint43/slave.make <host dir>

This will download Drupal core, contrib and custom modules, features, our installation profile and libraries needed to build and setup our slave sites.


Easy module listing:
```
drush pml --fields=name,type,status,package --status=enabled --no-core --field-labels=0 | grep -v Helsekompetanse | sort
```
