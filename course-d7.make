; Drupal Core
core = 7.x


api = 2
projects[drupal][version] = "7.69"
; See https://www.drupal.org/node/1123866
projects[drupal][patch][] = "https://www.drupal.org/files/issues/D7-forum_topic_err-1123866-24-do-no-test.patch"
; See https://www.drupal.org/node/1973278
projects[drupal][patch][] = "https://www.drupal.org/files/issues/image-accommodate_missing_definition-1973278-16.patch"

; Profiles
projects[slave][type] = "profile"
projects[slave][download][type] = "git"
projects[slave][download][url] = "git@bitbucket.org:Helsekompetanse/profile-course.git"
projects[slave][download][tag] = "prod_20200103"

; Modules
projects[adminimal_admin_menu][subdir] = "contrib"
projects[adminimal_admin_menu][version] = "1.9"
projects[admin_menu][subdir] = "contrib"
projects[admin_menu][version] = "3.0-rc6"
projects[admin_views][subdir] = "contrib"
projects[admin_views][version] = "1.7"
; Too many bugs introduced with this module when upgrading, check relase notes
; for known issues before upgrading. (TODO: Remove advagg module.)
; https://www.drupal.org/project/advagg/releases
projects[advagg][subdir] = "contrib"
projects[advagg][version] = "2.26"
projects[advanced_forum][subdir] = "contrib"
projects[advanced_forum][version] = "2.8"
projects[bakery][subdir] = "contrib"
projects[bakery][type] = "module"
; bakery does not release very often. Checkout from HEAD of dev to get important fixes.
; 2018-12-03: moved revision to current HEAD
projects[bakery][download][type] = "git"
projects[bakery][download][revision] = "35d3742eeb8dabc377196d4b409ad0fa2041f367"
projects[bakery][download][branch] = "7.x-2.x"
; book_helper update to 1.0 postponed since it's fairly recent update and contain several fixes. Delete this message after update. -JHH 260419.
projects[book_helper][subdir] = "contrib"
projects[book_helper][version] = "1.1"
projects[certificate][subdir] = "contrib"
projects[certificate][version] = "2.4"
projects[chart][subdir] = "contrib"
projects[chart][version] = "1.1"
projects[charts][subdir] = "contrib"
projects[charts][version] = "2.1"
projects[ckeditor_link][subdir] = "contrib"
projects[ckeditor_link][version] = "2.4"
projects[cpn][subdir] = "contrib"
projects[cpn][version] = "1.7"
projects[ctools][subdir] = "contrib"
projects[ctools][version] = "1.15"
projects[course][subdir] = "contrib"
projects[course][version] = "1.14"
projects[devel][subdir] = "contrib"
projects[devel][version] = "1.7"
projects[diff][subdir] = "contrib"
projects[diff][version] = "3.4"
projects[ds][subdir] = "contrib"
projects[ds][version] = "2.16"
projects[entity][subdir] = "contrib"
projects[entity][version] = "1.9"
projects[entity_modified][subdir] = "contrib"
projects[entity_modified][version] = "1.2"
projects[environment][subdir] = "contrib"
projects[environment][version] = "1.0"
projects[expire][subdir] = "contrib"
projects[expire][version] = "2.0-rc4"
projects[features][subdir] = "contrib"
projects[features][version] = "2.11"
projects[features_extra][subdir] = "contrib"
projects[features_extra][version] = "1.0"
projects[features_diff][subdir] = "contrib"
projects[features_diff][version] = "1.0-beta2"
projects[features_override][subdir] = "contrib"
projects[features_override][version] = "2.0-rc3"
projects[fences][subdir] = "contrib"
projects[fences][version] = "1.2"
projects[file_entity][subdir] = "contrib"
projects[file_entity][version] = "2.27"
projects[filebrowser][subdir] = "contrib"
projects[filebrowser][version] = "3.5"
projects[fitvids][subdir] = "contrib"
projects[fitvids][version] = "1.17"
projects[footable][subdir] = "contrib"
projects[footable][version] = "1.0"
; Maintainer seems to have dropped support for 1.x for some reason.
; Unsure if it's worth upgrading to 2.0 if it's not broken.
; -jhh 20191031
projects[form_builder][subdir] = "contrib"
projects[form_builder][version] = "1.22"
; This module have a track record of many upgrade related issues,
; always check releases and bug tracker before upgrading.
; https://www.drupal.org/project/google_tag/releases
projects[google_tag][subdir] = "contrib"
projects[google_tag][version] = "1.6"
projects[h5p][subdir] = "contrib"
projects[h5p][version] = "1.46"
projects[httprl][subdir] = "contrib"
projects[httprl][version] = "1.14"
projects[i18n][subdir] = "contrib"
projects[i18n][version] = "1.26"
projects[imagecache_actions][subdir] = "contrib"
projects[imagecache_actions][version] = "1.11"
projects[jquery_update][version] = "2.7"
projects[jquery_update][subdir] = "contrib"
; l10n_update 2.3 is affected by the following bug.
; https://www.drupal.org/project/l10n_update/issues/3088241
; Fix manually for now, only affects sites migrating to 7210.
; -jhh 20191101
projects[l10n_update][version] = "2.3"
projects[l10n_update][subdir] = "contrib"
;
; Libraries 2.4 upgrade can wait a bit, as it introduce many new features. -jhh.
projects[libraries][version] = "2.5"
projects[libraries][subdir] = "contrib"
projects[linkchecker][version] = "1.4"
projects[linkchecker][subdir] = "contrib"
projects[media][subdir] = "contrib"
projects[media][version] = "2.24"
projects[media_vimeo][subdir] = "contrib"
projects[media_vimeo][version] = "2.1"
projects[media_youtube][subdir] = "contrib"
projects[media_youtube][version] = "3.9"
; Metatag 7.x-1.26 is huge and recent, postponed for now. -jhh 20200103
; Details: https://www.drupal.org/project/metatag/releases/7.x-1.26
projects[metatag][subdir] = "contrib"
projects[metatag][version] = "1.26"
projects[mmenu][subdir] = "contrib"
projects[mmenu][version] = "2.1"
projects[module_filter][subdir] = "contrib"
projects[module_filter][version] = "2.2"
projects[multiform][subdir] = "contrib"
projects[multiform][version] = "1.1"
projects[node_clone][subdir] = "contrib"
projects[node_clone][version] = "1.0"
projects[options_element][subdir] = "contrib"
projects[options_element][version] = "1.12"
projects[panelizer][version] = "3.4"
projects[panelizer][subdir] = "contrib"
projects[panels][version] = "3.9"
projects[panels][subdir] = "contrib"
projects[panels_breadcrumbs][version] = "2.4"
projects[panels_breadcrumbs][subdir] = "contrib"
projects[panels_css_js][version] = "1.1"
projects[panels_css_js][subdir] = "contrib"
projects[pathauto][subdir] = "contrib"
projects[pathauto][version] = "1.3"
projects[plupload][subdir] = "contrib"
projects[plupload][version] = "1.7"
projects[print][subdir] = "contrib"
projects[print][version] = "2.2"
; Protected_node added for custom requirements on dafactory.helsekompetanse.no. 2017.09.14.
projects[protected_node][subdir] = "contrib"
projects[protected_node][version] = "1.4"
projects[purge][subdir] = "contrib"
projects[purge][version] = "1.7"
projects[quiz][subdir] = "contrib"
projects[quiz][version] = "5.6"
projects[quizfileupload][subdir] = "contrib"
projects[quizfileupload][version] = "5.3"
projects[realname][version] = "1.4"
projects[realname][subdir] = "contrib"
projects[redirect][version] = "1.0-rc3"
projects[redirect][subdir] = "contrib"
projects[redirect_token][version] = "1.0"
projects[redirect_token][subdir] = "contrib"
projects[render_cache][subdir] = "contrib"
projects[render_cache][version] = "1.0"
projects[retina_images][version] = "1.0-beta5"
projects[retina_images][subdir] = "contrib"
projects[rules][version] = "2.12"
projects[rules][subdir] = "contrib"
projects[search_autocomplete][version] = "4.9"
projects[search_autocomplete][subdir] = "contrib"
projects[services][version] = "3.12"
projects[services][subdir] = "contrib"
projects[strongarm][version] = "2.0"
projects[strongarm][subdir] = "contrib"
projects[system_stream_wrapper][subdir] = "contrib"
projects[system_stream_wrapper][version] = "1.0-rc1"
projects[taxonomy_menu][subdir] = "taxonomy_menu"
projects[taxonomy_menu][version] = "1.6"
projects[token][version] = "1.7"
projects[token][subdir] = "contrib"
projects[uuid][subdir] = "contrib"
projects[uuid][version] = "1.3"
projects[variable][subdir] = "contrib"
projects[variable][version] = "2.5"
projects[varnish][subdir] = "contrib"
projects[varnish][version] = "1.1"
projects[video_embed_field][subdir] = "contrib"
projects[video_embed_field][version] = "2.0-beta11"
projects[view_unpublished][subdir] = "contrib"
projects[view_unpublished][version] = "1.2"
projects[views][subdir] = "contrib"
projects[views][version] = "3.23"
projects[views_bulk_operations][subdir] = "contrib"
projects[views_bulk_operations][version] = "3.5"
projects[views_data_export][subdir] = "contrib"
projects[views_data_export][version] = "3.2"
projects[webform][subdir] = "contrib"
projects[webform][version] = "4.21"
projects[wysiwyg][subdir] = "contrib"
projects[wysiwyg][version] = "2.6"
projects[xautoload][subdir] = "contrib"
projects[xautoload][version] = "5.7"
projects[hotjar][subdir] = "contrib"
projects[hotjar][version] = "1.3"
projects[raven][subdir] = "contrib"
projects[raven][version] = "2.16"
projects[highlight][subdir] = "contrib"
projects[highlight][version] = "1.x-dev"


; External modules
projects[res_images][subdir] = "external"
projects[res_images][type] = "module"
projects[res_images][download][type] = "git"
projects[res_images][download][url]="git://github.com/oleanti/res_images.git"

; Custom modules
projects[hknodecontent][subdir] = "custom"
projects[hknodecontent][type] = "module"
projects[hknodecontent][download][type] = "git"
projects[hknodecontent][download][url]="https://bitbucket.org/Helsekompetanse/hknodecontent.git"
projects[hknodecontent][download][tag]="prod_20200103"

projects[hk_environments][subdir] = "custom"
projects[hk_environments][type] = "module"
projects[hk_environments][download][type] = "git"
projects[hk_environments][download][url]="git@bitbucket.org:Helsekompetanse/hk_environments.git"
projects[hk_environments][download][tag]="prod_20200103"

projects[hk_l10n_conf][subdir] = "custom"
projects[hk_l10n_conf][type] = "module"
projects[hk_l10n_conf][download][type] = "git"
projects[hk_l10n_conf][download][url]="git@bitbucket.org:Helsekompetanse/hk_l10n_conf.git"
projects[hk_l10n_conf][download][tag]="prod_20200103"

projects[varor][subdir] = "custom"
projects[varor][type] = "module"
projects[varor][download][type] = "git"
projects[varor][download][url] = "https://bitbucket.org/Helsekompetanse/varor.git"
projects[varor][download][tag] = "prod_20200103"

; Theme
projects[zurb_foundation][type] = "theme"
projects[zurb_foundation][version] = "5.0-rc6"
projects[zurb_foundation][subdir] = "contrib"

projects[adminimal_theme][type] = "theme"
projects[adminimal_theme][version] = "1.26"
projects[adminimal_theme][subdir] = "contrib"

; Libraries
libraries[ckeditor][download][type] = "file"
libraries[ckeditor][download][url] = "https://download.cksource.com/CKEditor/CKEditor/CKEditor%204.9.2/ckeditor_4.9.2_full.zip"

libraries[FooTable][download][type] = "file"
libraries[FooTable][download][url] = "https://github.com/fooplugins/FooTable/archive/V2.zip"

libraries[fitvids][download][type] = "file"
libraries[fitvids][download][url] = "https://raw.github.com/davatron5000/FitVids.js/master/jquery.fitvids.js"

libraries[plupload][download][type] = "file"
libraries[plupload][download][url] = "https://github.com/moxiecode/plupload/archive/v1.5.8.zip"

libraries[codemirror][download][type] = "file"
libraries[codemirror][download][url] = "http://codemirror.net/codemirror.zip"

libraries[mmenu][download][type] = "git"
libraries[mmenu][download][url] = "git@bitbucket.org:Helsekompetanse/mmenu.git"
libraries[mmenu][download][tag] = "prod_20200103"

libraries[hkapi][download][type] = "git"
libraries[hkapi][download][url] = "git@bitbucket.org:Helsekompetanse/hkapi.git"
libraries[hkapi][download][tag] = "prod_20200103"

libraries[tablesaw][download][type] = "file"
libraries[tablesaw][download][url] = "https://github.com/filamentgroup/tablesaw/releases/download/v1.0.4/tablesaw-1.0.4.zip"

libraries[mpdf][download][type] = "file"
libraries[mpdf][download][url] = "https://github.com/mpdf/mpdf/releases/download/v6.1.0/02-mPDF-v6.1.0-without-examples.zip"

libraries[sentry-php][download][type] = "file"
libraries[sentry-php][download][url] = "https://github.com/getsentry/sentry-php/archive/1.5.0.zip"

libraries[monolog][download][type] = "file"
libraries[monolog][download][url] = "https://github.com/Seldaek/monolog/archive/1.21.0.zip"

libraries[log][download][type] = "file"
libraries[log][download][url] = "https://github.com/php-fig/log/archive/1.0.1.zip"

; Translations
; Commented out for development
translations[] = nb
