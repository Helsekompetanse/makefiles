; This is the platform that was used for a couple of one-off sites back in the
; day. Hopefully we can kill these or atleast re-implement them on our course
; platform some day.

core = 7.x

api = 2
projects[drupal][version] = "7.69"

; Modules
projects[admin_menu][subdir] = "contrib"
projects[admin_menu][version] = "3.0-rc6"
projects[adminimal_admin_menu][subdir] = "contrib"
projects[adminimal_admin_menu][version] = "1.9"
projects[bakery][subdir] = "contrib"
projects[bakery][version] = "2.0-alpha4"
projects[bakery][patch][] = "https://www.drupal.org/files/1916610-bakery-validate-d7.patch"
projects[ctools][subdir] = "contrib"
projects[ctools][version] = "1.15"
projects[coffee][subdir] = "contrib"
projects[coffee][version] = "2.3"
projects[context][subdir] = "contrib"
projects[context][version] = "3.10"
projects[cpn][subdir] = "contrib"
projects[cpn][version] = "1.7"
projects[devel][subdir] = "contrib"
projects[devel][version] = "1.7"
projects[diff][subdir] = "contrib"
projects[diff][version] = "3.4"
projects[ds][subdir] = "contrib"
projects[ds][version] = "2.16"
projects[entity][subdir] = "contrib"
projects[entity][version] = "1.9"
projects[file_entity][subdir] = "contrib"
projects[file_entity][version] = "2.27"
; Maintainer seems to have dropped support for 1.x for some reason.
; Unsure if it's worth upgrading to 2.0 if it's not broken.
; -jhh 20191031
projects[form_builder][subdir] = "contrib"
projects[form_builder][version] = "1.22"
projects[google_analytics][subdir] = "contrib"
projects[google_analytics][version] = "2.6"
projects[hacked][subdir] = "contrib"
projects[hacked][version] = "2.0-beta8"
projects[jquery_update][subdir] = "contrib"
projects[jquery_update][version] = "2.7"
projects[l10n_update][subdir] = "contrib"
projects[l10n_update][version] = "1.4"
projects[libraries][subdir] = "contrib"
projects[libraries][version] = "2.5"
projects[media][subdir] = "contrib"
projects[media][version] = "2.24"
projects[media_vimeo][subdir] = "contrib"
projects[media_vimeo][version] = "2.1"
projects[module_filter][subdir] = "contrib"
projects[module_filter][version] = "2.2"
projects[multiupload_filefield_widget][subdir] = "contrib"
projects[multiupload_filefield_widget][version] = "1.13"
projects[options_element][subdir] = "contrib"
projects[options_element][version] = "1.12"
projects[panels][subdir] = "contrib"
projects[panels][version] = "3.9"
projects[pathauto][subdir] = "contrib"
projects[pathauto][version] = "1.3"
projects[redirect][subdir] = "contrib"
projects[redirect][version] = "1.0-rc3"
projects[token][subdir] = "contrib"
projects[token][version] = "1.7"
projects[varnish][subdir] = "contrib"
projects[varnish][version] = "1.4"
projects[views][subdir] = "contrib"
projects[views][version] = "3.23"
projects[webform][subdir] = "contrib"
projects[webform][version] = "3.29"
projects[webform_anonymous][subdir] = "contrib"
projects[webform_anonymous][version] = "1.0-beta1"
projects[webform_report][subdir] = "contrib"
projects[webform_report][version] = "1.0"
projects[wysiwyg][subdir] = "contrib"
projects[wysiwyg][version] = "2.6"

; Themes
projects[zen][version] = "5.6"

; Modules
; Please fill the following out. Type may be one of get, git, bzr or svn,
; and url is the url of the download.
projects[autismeportalen_module][download][type] = ""
projects[autismeportalen_module][download][url] = ""
projects[autismeportalen_module][type] = "module"

projects[hk_environments][type] = "module"
projects[hk_environments][download][type] = "git"
projects[hk_environments][download][url] = "git@bitbucket.org:Helsekompetanse/hk_environments.git"
projects[hk_environments][download][revision] = "bf91ab64480e33b14acf7421c2955bc845327a9a"

projects[res_images][type] = "module"
projects[res_images][download][type] = "git"
projects[res_images][download][url] = "https://github.com/oleanti/res_images.git"

projects[some_field][type] = "module"
projects[some_field][download][type] = "git"
projects[some_field][download][url] = "https://github.com/oleanti/some_field.git"

; Libraries
libraries[modernizr][download][type] = "file"
libraries[modernizr][download][url] = "https://bitbucket.org/Helsekompetanse/makefiles/raw/79d64cc572da8ef3a1ea509eb19ad999a94d270b/files/libraries/modernizr.js"
libraries[magnific-popup][download][type] = "git"
libraries[magnific-popup][download][tag] = "1.0.0"
libraries[magnific-popup][download][url] = "https://github.com/dimsemenov/Magnific-Popup.git"
libraries[chosen][download][type] = "git"
libraries[chosen][download][tag] = "1.4.2"
libraries[chosen][download][url] = "https://github.com/harvesthq/chosen.git"
libraries[fitvids][download][type] = "git"
libraries[fitvids][download][tag] = "v1.1.0"
libraries[fitvids][download][url] = "https://github.com/davatron5000/FitVids.js.git"
libraries[ckeditor][download][type] = "file"
libraries[ckeditor][download][url] = "https://download.cksource.com/CKEditor/CKEditor/CKEditor%204.9.2/ckeditor_4.9.2_full.zip"
libraries[ckeditor][directory_name] = "ckeditor"
libraries[ckeditor][type] = "library"

; WARNING!
; There is a good chance that the libraries below aren't used for much!
; Unfortunately, I don't have time to check thoroughly.

; Please fill the following out. Type may be one of get, git, bzr or svn,
; and url is the url of the download.
libraries[codemirror][download][type] = "file"
libraries[codemirror][download][url] = "http://codemirror.net/codemirror.zip"
libraries[codemirror][directory_name] = "codemirror"
libraries[codemirror][type] = "library"

; Please fill the following out. Type may be one of get, git, bzr or svn,
; and url is the url of the download.
libraries[underscore][download][type] = "file"
libraries[underscore][download][url] = "https://github.com/jashkenas/underscore/archive/master.zip"
libraries[underscore][directory_name] = "underscore"
libraries[underscore][type] = "library"

; Please fill the following out. Type may be one of get, git, bzr or svn,
; and url is the url of the download.
libraries[getlocations][download][type] = "file"
libraries[getlocations][download][url] = "https://bitbucket.org/Helsekompetanse/makefiles/raw/470e8cf6dc1f0529b77f8bb9b5004ba3988db0f0/libraries/getlocations-markers.zip"
libraries[getlocations][directory_name] = "getlocations/markers"
libraries[getlocations][type] = "library"

; Please fill the following out. Type may be one of get, git, bzr or svn,
; and url is the url of the download.
libraries[backbone][download][type] = "file"
libraries[backbone][download][url] = "https://github.com/jashkenas/backbone/archive/master.zip"
libraries[backbone][directory_name] = "backbone"
libraries[backbone][type] = "library"

