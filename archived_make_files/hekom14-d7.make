core = 7.x

api = 2
projects[drupal][version] = "7.32"

; Modules
projects[admin_menu][subdir] = "contrib"
projects[admin_menu][version] = "3.0-rc4"

projects[adminimal_admin_menu][subdir] = "contrib"
projects[adminimal_admin_menu][version] = "1.5"

projects[advagg][subdir] = "contrib"
projects[advagg][version] = "2.5"

projects[bakery][subdir] = "contrib"
projects[bakery][version] = "2.0-alpha4"

projects[block_class][subdir] = "contrib"
projects[block_class][version] = "2.1"

projects[ctools][subdir] = "contrib"
projects[ctools][version] = "1.3"

projects[context][subdir] = "contrib"
projects[context][version] = "3.1"

projects[date][subdir] = "contrib"
projects[date][version] = "2.7"

projects[devel][subdir] = "contrib"
projects[devel][version] = "1.3"

projects[expire][subdir] = "contrib"
projects[expire][version] = "2.0-rc2"

projects[environment][subdir] = "contrib"
projects[environment][version] = "1.0"

projects[fontyourface][subdir] = "contrib"
projects[fontyourface][version] = "2.8"

projects[features][subdir] = "contrib"
projects[features][version] = "2.0"

projects[media][subdir] = "contrib"
projects[media][version] = "1.4"

projects[form_builder][subdir] = "contrib"
projects[form_builder][version] = "1.6"

projects[l10n_update][subdir] = "contrib"
projects[l10n_update][version] = "1.0-beta3"

projects[menu_attributes][subdir] = "contrib"
projects[menu_attributes][version] = "1.0-rc2"

projects[menu_block][subdir] = "contrib"
projects[menu_block][version] = "2.3"

projects[metatag][subdir] = "contrib"
projects[metatag][version] = "1.0-beta9"

projects[module_filter][subdir] = "contrib"
projects[module_filter][version] = "2.0-alpha2"

projects[options_element][subdir] = "contrib"
projects[options_element][version] = "1.10"

projects[panels][subdir] = "contrib"
projects[panels][version] = "3.3"

projects[retina_images][subdir] = "contrib"
projects[retina_images][version] = "1.0-beta4"

projects[purge][subdir] = "contrib"
projects[purge][version] = "1.6"

projects[token][subdir] = "contrib"
projects[token][version] = "1.5"

projects[varnish][subdir] = "contrib"
projects[varnish][version] = "1.0-beta3"

projects[varor][subdir] = "custom"
projects[varor][type] = "module"
projects[varor][download][type] = "git"
projects[varor][download][url] = "https://bitbucket.org/Helsekompetanse/varor.git"
projects[varor][download][tag] = "20150831"

projects[views][subdir] = "contrib"
projects[views][version] = "3.7"

projects[webform][subdir] = "contrib"
projects[webform][version] = "3.19"

projects[wysiwyg][subdir] = "contrib"
projects[wysiwyg][version] = "2.2"

projects[varnish][subdir] = "contrib"
projects[varnish][version] = "1.0-beta2"

projects[xautoload][subdir] = "contrib"
projects[xautoload][version] = "5.2"


; Themes
projects[adminimal_theme][version] = "1.18"

projects[bootstrap][version] = "3.0"

projects[cti_flex][version] = "1.0"

projects[cube][version] = "1.4"

projects[rubik][version] = "4.0-rc1"

projects[tao][version] = "3.0-beta4"

projects[zen][version] = "5.4"
